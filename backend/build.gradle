plugins {
    id 'java'
    id 'org.springframework.boot' version '3.0.4'
    id 'io.spring.dependency-management' version '1.1.0'
    id 'jacoco'
    id 'org.sonarqube' version '4.0.0.2929'
}

group = 'com.example'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '17'
targetCompatibility = '17'

repositories {
    mavenCentral()
}

configurations {
    compileOnly {
        extendsFrom annotationProcessor
    }
    developmentOnly
    runtimeClasspath {
        extendsFrom developmentOnly
    }
}

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-actuator'
    implementation 'org.springframework.boot:spring-boot-starter-jdbc'
    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    implementation 'org.springframework.boot:spring-boot-starter-security'
    implementation 'org.springframework.boot:spring-boot-starter-validation'
    implementation('org.springframework.boot:spring-boot-starter-web') {
        exclude module: 'spring-boot-starter-tomcat'
    }
    implementation('org.springframework.boot:spring-boot-starter-undertow') {
        exclude module: 'undertow-websockets-jsr'
    }
    implementation 'io.jsonwebtoken:jjwt-api:0.11.5'
    implementation 'io.jsonwebtoken:jjwt-impl:0.11.5'
    implementation 'io.jsonwebtoken:jjwt-jackson:0.11.5'
    implementation 'org.bgee.log4jdbc-log4j2:log4jdbc-log4j2-jdbc4.1:1.16'
    implementation 'com.zaxxer:HikariCP:5.0.1'
    implementation 'org.liquibase:liquibase-core:4.19.1'
    implementation 'org.springdoc:springdoc-openapi-starter-webmvc-ui:2.0.4'
    implementation 'io.opentelemetry.instrumentation:opentelemetry-instrumentation-annotations:1.26.0'
    testImplementation 'org.projectlombok:lombok:1.18.22'
    compileOnly 'org.projectlombok:lombok:1.18.26'
    runtimeOnly 'org.postgresql:postgresql:42.5.4'
    //runtimeOnly 'com.h2database:h2:2.1.214'
    annotationProcessor 'org.projectlombok:lombok'
    annotationProcessor 'org.springframework.boot:spring-boot-configuration-processor'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'
    testImplementation 'org.springframework.security:spring-security-test:6.0.2'
}

jacoco {
    toolVersion = '0.8.8'
}

jacocoTestReport {
    reports {
        html.required = true
        xml.required = true
        csv.required = false
        html.outputLocation = layout.buildDirectory.dir('jacoco/jacocoHtml')
        xml.outputLocation = layout.buildDirectory.dir('jacoco').get().file('jacoco.xml')
    }
}

test {
    finalizedBy jacocoTestReport // report is always generated after tests run
}

jacocoTestReport {
    dependsOn test // tests are required to run before generating the report
    finalizedBy jacocoTestCoverageVerification // verification is always run after report is generated
}

jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                counter = 'INSTRUCTION'
                value = 'COVEREDRATIO'
                minimum = 0.30
            }
        }
    }
}

tasks.named('jar') {
    enabled = false
}

tasks.named('test') {
    useJUnitPlatform()
}
