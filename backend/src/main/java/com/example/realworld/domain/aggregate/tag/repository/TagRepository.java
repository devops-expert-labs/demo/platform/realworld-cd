package com.example.realworld.domain.aggregate.tag.repository;

import com.example.realworld.domain.aggregate.tag.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {
}
