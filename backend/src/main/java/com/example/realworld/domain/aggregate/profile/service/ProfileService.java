package com.example.realworld.domain.aggregate.profile.service;

import com.example.realworld.domain.aggregate.profile.dto.ProfileResponse;
import com.example.realworld.domain.aggregate.user.dto.UserAuth;

public interface ProfileService {
    ProfileResponse getProfile(UserAuth userAuth, String username);

    ProfileResponse followUser(UserAuth userAuth, String username);

    ProfileResponse unfollowUser(UserAuth userAuth, String username);
}
